import { Component, OnInit } from '@angular/core';
import { IDivision } from '../models/Division';
import { IOffice } from '../models/Office';
import { IPosition, Position } from '../models/position';
import  {ISalary, Salary}  from '../models/Salary'
import {DataService} from '../services/data.service'
@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.css']
})
export class SalaryComponent implements OnInit {  

  salaries : Salary[] = [];
  employee : Salary = new Salary();
  positions : IPosition[] = [];
  divisions : IDivision[] = [];
  offices : IOffice[] = [];
  errors : string[] = [];
  messaje: string = ''

  constructor(private DataService: DataService) { }

  ngOnInit(): void {    
    this.GetPosition();
    this.GetDivision();
    this.GetOffice();
  };

public AddSalary(){
  var newSalary = new Salary();
    newSalary.employeeCode = this.employee.employeeCode;
          newSalary.employeeName = this.employee.employeeName;
          newSalary.employeeSurName = this.employee.employeeSurName;
          newSalary.identificationNumber = this.employee.identificationNumber;
          newSalary.officeId = this.employee.officeId;
          newSalary.positionId = this.employee.positionId;
          newSalary.divisionId = this.employee.divisionId;
          newSalary.grade = this.employee.grade;
  this.salaries.push(newSalary);
  this.Validate();
}

public Order (){
  this.salaries = this.salaries.sort( function(a, b) {          
      if (a.year === b.year) {         
         return a.month - b.month;
      }
      return a.year < b.year ? 1 : -1;
   });
   this.Validate();
}
  public Validate(): boolean{     
    this.errors = [];
    this.messaje = '';
    if(this.employee.employeeCode.trim() == '')
    {
      this.errors.push('employeeCode');      
    }
    if(this.employee.employeeName.trim() == '')
    {
      this.errors.push('employeeName');      
    }
    if(this.employee.employeeSurName.trim() == '')
    {
      this.errors.push('employeeSurName');      
    }
    if(this.employee.identificationNumber.trim() == '')
    {
      this.errors.push('identificationNumber');      
    }
    if(this.employee.officeId == 0)
    {
      this.errors.push('officeId');      
    }
     if(this.employee.divisionId == 0)
    {
      this.errors.push('divisionId');      
    }    
     if(this.employee.positionId == 0)
    {
      this.errors.push('positionId');      
    }
      if(this.employee.grade == 0)
    {
      this.errors.push('grade');      
    }
    console.log(this.employee);
    console.log(this.errors);

    var result: boolean = true;          
    for (let salary of this.salaries) {
        salary.yearError = 0;
        salary.monthError = 0;
      if (salary.year == 0) {
        salary.yearError = 1;
        result = false;
      }
      if (salary.month == 0) {
        salary.monthError = 1;
        result = false;
      }
      if (this.salaries.filter(f=> f.year == salary.year && f.month == salary.month).length > 1)
      {
        salary.yearError = 1;
        salary.monthError = 1;
        result = false;
      }           
    }
    return result && this.errors.length == 0;    
   }


  public Save(){     
    if (this.Validate())
    {    
    for (let salary of this.salaries) {        
     this.DataService.SetSalary(salary).subscribe(result =>{ 
       salary.id = result.id;  
       this.messaje = "Data has been saved successfully."
    });    
    }

    }
   }
  public GetSalary(){    

     this.DataService.GetSalary(this.employee).subscribe(result =>{
       this.salaries = [];
       this.messaje = "";
        for (let salary of result.salaryList)
        {

          this.salaries.push(salary);
        }         
        if (result.salaryList.length > 0)
        {
            this.employee = result.salaryList[0];
            this.Validate();
        }
        else
        {
          this.employee = new Salary();
          this.errors = [];
          this.salaries = [];
          this.messaje = "";
        }              
    });
    }  
    
     public GetPosition(){
     this.DataService.GetPosition().subscribe(result =>{
       this.positions = [];
        for (let Position of result.positionList)
        {
          this.positions.push(Position);
        }                
    });
     }

     public GetOffice(){
     this.DataService.GetOffice().subscribe(result =>{
       this.offices = [];
        for (let Office of result.officeList)
        {
          this.offices.push(Office);
        }                
    });
  }

     public GetDivision(){
     this.DataService.GetDivision().subscribe(result =>{
       this.divisions = [];
        for (let Division of result.divisionList)
        {
          this.divisions.push(Division);
        }                
    });
    }  
    public NewSalary(){
      this.employee = new Salary();
      this.salaries = [];
      this.messaje = '';
    }    
    public Delete (salary: Salary)
    {
      this.employee = salary;
      if(salary.id != 0)
      {
          this.DataService.DeleteSalary(salary).subscribe(result =>{ 
            if(result.isValid)           
            {
              this.messaje = "Data has been saved successfully."
            }
    });
       
      }
          this.salaries = this.salaries.filter( s => s != salary)
    }
}

