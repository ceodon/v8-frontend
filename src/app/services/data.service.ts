import { SalaryModel, SalaryListModel } from './../models/view-model/salary.model';
import { ISalary } from '../models/salary';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import {HttpClient} from "@angular/common/http";
import { IPosition } from '../models/position';
import { PositionListModel } from '../models/view-model/position.model';
import { DivisionListModel } from '../models/view-model/division.model';
import { OfficeListModel } from '../models/view-model/office.model';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiUrl: string = 'http://localhost:61123'

  constructor(private httpClient:HttpClient) { }

  
  public SetSalary(salary: ISalary): Observable<SalaryModel> {
    let endPoint = this.apiUrl +'/Salary/set-salary';
    return this.httpClient
      .post(endPoint,salary)
      .pipe(map((response: any) => {        
        if (response) {          
          let model = response as SalaryModel;
          if (model.isValid) {
            return model;
          }
          else {
            throw model.message;
          }
        }
        else {
          console.log("Empty result");
        }
      }),        
      );
  }

   public GetSalary(salary: ISalary): Observable<SalaryListModel> {
    let endPoint = this.apiUrl +'/Salary/get-salary-by-employee';
    return this.httpClient
      .post(endPoint,salary)
      .pipe(map((response: any) => {        
        if (response) {       
             
          let model = response as SalaryListModel;
          if (model.isValid) {
            return model;
          }
          else {
            throw model.message;
          }
        }
        else {
          console.log("Empty result");
        }
      }),        
      );
  }

     public GetPosition(): Observable<PositionListModel> {
    let endPoint = this.apiUrl +'/Salary/get-position';
    return this.httpClient
      .post(endPoint, null)
      .pipe(map((response: any) => {        
        if (response) {          
          let model = response as PositionListModel;
          if (model.isValid) {
            return model;
          }
          else {
            throw model.message;
          }
        }
        else {
          console.log("Empty result");
        }
      }),        
      );      
  }

     public GetDivision(): Observable<DivisionListModel> {
    let endPoint = this.apiUrl +'/Salary/get-Division';
    return this.httpClient
      .post(endPoint, null)
      .pipe(map((response: any) => {        
        if (response) {          
          let model = response as DivisionListModel;
          if (model.isValid) {
            return model;
          }
          else {
            throw model.message;
          }
        }
        else {
          console.log("Empty result");
        }
      }),        
      );      
  }

     public GetOffice(): Observable<OfficeListModel> {
    let endPoint = this.apiUrl +'/Salary/get-Office';
    return this.httpClient
      .post(endPoint, null)
      .pipe(map((response: any) => {        
        if (response) {          
          let model = response as OfficeListModel;
          if (model.isValid) {
            return model;
          }
          else {
            throw model.message;
          }
        }
        else {
          console.log("Empty result");
        }
      }),        
      );      
  }

    public DeleteSalary(salary: ISalary): Observable<SalaryModel> {
    let endPoint = this.apiUrl +'/Salary/delete-salary';
    return this.httpClient
      .post(endPoint,salary)
      .pipe(map((response: any) => {        
        if (response) {          
          let model = response as SalaryModel;
          if (model.isValid) {
            return model;
          }
          else {
            throw model.message;
          }
        }
        else {
          console.log("Empty result");
        }
      }),        
      );
  }

}
