export interface IPosition {
    id: number;       
    name: string;
    isActive: boolean
    
  }

export class Position implements IPosition{
    name = '';
    isActive = true;
    id: number = 0;    
   
  }  