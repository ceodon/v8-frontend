export interface ISalary {
    id: number;    
    year: number;
    month: number;    
    officeId: number;
    employeeCode: string;
    employeeName: string;
    employeeSurName: string;
    divisionId: number;
    positionId: number;
    grade: number;
    beginDate : Date;
    birthday : Date;
    identificationNumber: string;
    baseSalary: number;
    productionBonus: number;
    compensationBonus: number;
    commission: number;
    contributions: number; 
    
    yearError:number;
    monthError:number;
  }

export class Salary implements ISalary{
    id = 0;    
    year= 0;
    month= 0;
    officeId =  0;
    employeeCode= '';
    employeeName= '';
    employeeSurName= '';
    divisionId = 0;
    positionId = 0;
    grade = 0;
    beginDate : Date;
    birthday : Date;
    identificationNumber= '';
    baseSalary = 0;
    productionBonus = 0;
    compensationBonus = 0;
    commission = 0;
    contributions = 0;
    yearError = 0;
    monthError = 0;
  }  