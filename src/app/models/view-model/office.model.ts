import { BaseModel } from './base.model';
import { IOffice, Office } from '../Office';

export class OfficeModel extends BaseModel {
    Office: IOffice;
    constructor (){
        super();
        this.Office = new Office();
    }
}

export class OfficeListModel extends BaseModel {
    officeList: IOffice[];
    constructor (){
        super();
        this.officeList = [];
    }
}

