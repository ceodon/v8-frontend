import { BaseModel } from './base.model';
import { ISalary, Salary } from './../salary';

export class SalaryModel extends BaseModel {
    salary: ISalary;
    constructor (){
        super();
        this.salary = new Salary();
    }
}

export class SalaryListModel extends BaseModel {
    salaryList: ISalary[];
    constructor (){
        super();
        this.salaryList = [];
    }
}

