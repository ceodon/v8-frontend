export class BaseModel {
    id: number;
    isValid: boolean;
    message: string;
    constructor (){
        this.id = 0;
        this.isValid = true;
        this.message = "";
    }
}