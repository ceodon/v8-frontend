import { BaseModel } from './base.model';
import { IDivision, Division } from '../Division';

export class DivisionModel extends BaseModel {
    Division: IDivision;
    constructor (){
        super();
        this.Division = new Division();
    }
}

export class DivisionListModel extends BaseModel {
    divisionList: IDivision[];
    constructor (){
        super();
        this.divisionList = [];
    }
}

