import { BaseModel } from './base.model';
import { IPosition, Position } from '../position';

export class PositionModel extends BaseModel {
    Position: IPosition;
    constructor (){
        super();
        this.Position = new Position();
    }
}

export class PositionListModel extends BaseModel {
    positionList: IPosition[];
    constructor (){
        super();
        this.positionList = [];
    }
}

