export interface IOffice {
    id: number;       
    name: string;
    isActive: boolean
    
  }

export class Office implements IOffice{
    name = '';
    isActive = true;
    id: number = 0;    
   
  }  