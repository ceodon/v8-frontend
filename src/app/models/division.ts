export interface IDivision {
    id: number;       
    name: string;
    isActive: boolean
    
  }

export class Division implements IDivision{
    name = '';
    isActive = true;
    id: number = 0;    
   
  }  