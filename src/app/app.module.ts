import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NavegacionComponent}  from './navegacion/navegacion.component'
import { SalaryComponent } from './salary/salary.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
//import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
    ,NavegacionComponent
    ,SalaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    //BrowserAnimationsModule,
    //ToastrModule.forRoot(),
  ],
   exports: [     
    NavegacionComponent
    ,SalaryComponent
  ],
    
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
